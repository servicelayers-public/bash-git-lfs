FROM bash

# Add user 1000 as a member of root group
RUN adduser -u 1000 -g "" -G root -D app &&\
  apk add --update --no-cache \
    git \
    git-lfs

USER 1000

ARG IMAGE_COMMIT_ARG=""
ENV IMAGE_COMMIT=${IMAGE_COMMIT_ARG}
ARG IMAGE_TAG_ARG=""
ENV IMAGE_TAG=${IMAGE_TAG_ARG}